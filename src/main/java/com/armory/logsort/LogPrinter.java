package com.armory.logsort;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

public class LogPrinter {

    private final ChunkedLogAggregator logAggregator;

    public LogPrinter(ChunkedLogAggregator logAggregator) {
        this.logAggregator = logAggregator;
    }

    public void printLogs(List<File> logFiles) {
        sortLogs(logFiles).forEach(System.out::println);
    }

    protected List<String> sortLogs(List<File> logFiles) {
        try {
            return logAggregator
                    .sortAndAggregateLogsAsStream(logFiles)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            throw new RuntimeException("Aggregation failed with the reason: " + e.getMessage(), e);
        }
    }
}
