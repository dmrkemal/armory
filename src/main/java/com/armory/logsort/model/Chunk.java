package com.armory.logsort.model;

import java.util.ArrayList;
import java.util.List;

public class Chunk {

    private final int chunkNo;
    private final int capacity;
    private int currentCapacity;
    private final List<String> logs;

    public Chunk(int chunkNo, int capacity) {
        this.chunkNo = chunkNo;
        this.capacity = capacity;
        this.currentCapacity = 0;
        this.logs = new ArrayList<>();
    }

    public int getChunkNo() {
        return chunkNo;
    }

    public List<String> getLogs() {
        return logs;
    }

    public void addLog(String log) {
        logs.add(log);
        updateCurrentCapacity(log);
    }

    public boolean hasCapacity(String log) {
        return currentCapacity < capacity;
    }

    private void updateCurrentCapacity(String log) {
        currentCapacity += sizeOf(log);
    }

    private int sizeOf(String log) {
        return log.length() + 1;
    }
}
