package com.armory.logsort.model;

import java.time.ZonedDateTime;
import java.util.Comparator;
import java.util.Objects;

public class LogEntry {

    private final ZonedDateTime timestamp;
    private final String log;

    public LogEntry(String log) {
        this.timestamp = ZonedDateTime.parse(log.split(",")[0].trim());
        this.log = log;
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    public String getLog() {
        return log;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) return true;
        if (that == null || getClass() != that.getClass()) return false;
        LogEntry logEntry = (LogEntry) that;
        return log.equals(logEntry.log);
    }

    @Override
    public int hashCode() {
        return Objects.hash(log);
    }

    public static class LogEntryComparator implements Comparator<LogEntry> {

        @Override
        public int compare(LogEntry o1, LogEntry o2) {
            return o1.timestamp.compareTo(o2.timestamp);
        }
    }
}
