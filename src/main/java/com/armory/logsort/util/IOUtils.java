package com.armory.logsort.util;

import org.springframework.util.FileSystemUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public final class IOUtils {
    private IOUtils() {
    }

    public static void clearChunkDirectory(String chunkDirectoryPath) {
        File file = new File(chunkDirectoryPath);

        if (file.exists()) {
            boolean deleted = FileSystemUtils.deleteRecursively(file);
            if (!deleted) throw new IllegalArgumentException("Directory couldn't be deleted");
        }

        if (!file.exists() || !file.isDirectory()) {
            boolean created = file.mkdir();
            if (!created) throw new IllegalArgumentException("Directory couldn't be created");
        }
    }

    public static void writeLogsToFile(List<String> logs, File file) {
        try (BufferedWriter writer = Files.newBufferedWriter(file.toPath())) {
            for (String log : logs) {
                writer.write(log);
                writer.write(System.lineSeparator());
            }
            writer.flush();
        } catch (IOException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    public static Set<File> listFiles(String dir) throws IOException {
        return Files.list(Paths.get(dir))
                .filter(file -> !Files.isDirectory(file))
                .map(Path::toFile)
                .collect(Collectors.toSet());
    }
}
