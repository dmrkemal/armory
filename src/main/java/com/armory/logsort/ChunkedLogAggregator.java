package com.armory.logsort;


import com.armory.logsort.model.Chunk;
import com.armory.logsort.model.LogEntry;
import com.armory.logsort.util.IOUtils;
import org.springframework.util.FileSystemUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ChunkedLogAggregator {

    public static final int DEFAULT_CHUNK_CAPACITY = 10000;

    private static final String CHUNK_DIRECTORY = "./src/main/resources/chunk/";
    private static final String CHUNK_FILE_PREFIX = "chunk_";

    private final int chunkCapacity;

    public ChunkedLogAggregator(int chunkCapacity) {
        this.chunkCapacity = chunkCapacity;
    }

    public List<String> sortAndAggregateLogs(List<File> logFiles) throws IOException {
        return sortAndAggregateLogsAsStream(logFiles)
                .collect(Collectors.toList());
    }

    public Stream<String> sortAndAggregateLogsAsStream(List<File> logFiles) throws IOException {
        splitIntoChunks(logFiles);
        return sortAndAggregateChunksIntoStream();
    }

    private void splitIntoChunks(List<File> logFiles) {
        IOUtils.clearChunkDirectory(CHUNK_DIRECTORY);

        int chunkNo = 0;
        for (File logFile : logFiles) {
            Chunk chunk = new Chunk(++chunkNo, chunkCapacity);
            try (BufferedReader br = Files.newBufferedReader(logFile.toPath())) {
                String log;
                while ((log = br.readLine()) != null) {
                    if (chunk.hasCapacity(log)) chunk.addLog(log);
                    else {
                        writeLogsInChunkToFile(chunk);
                        chunk = new Chunk(++chunkNo, chunkCapacity);
                        chunk.addLog(log);
                    }
                }

                //write outstanding logs into file
                writeLogsInChunkToFile(chunk);
            } catch (IOException ex) {
                throw new RuntimeException(ex.getMessage(), ex);
            }
        }
    }

    private Stream<String> sortAndAggregateChunksIntoStream() throws IOException {
        try {
            Stream.Builder<String> sortedLogStreamBuilder = Stream.builder();
            Map<LogEntry, BufferedReader> logEntryMap = new HashMap<>();

            Set<File> chunkedFiles = IOUtils.listFiles(CHUNK_DIRECTORY);
            for (File chunkedFile : chunkedFiles) {
                BufferedReader reader = Files.newBufferedReader(chunkedFile.toPath());
                String log = reader.readLine();
                if (log != null) logEntryMap.put(new LogEntry(log), reader);
            }

            // loop until all the logs in chunks consumed
            List<LogEntry> sortedLogEntries = new LinkedList<>(logEntryMap.keySet());
            while (logEntryMap.size() > 0) {
                sortedLogEntries.sort(new LogEntry.LogEntryComparator());
                LogEntry logEntry = sortedLogEntries.remove(0);
                sortedLogStreamBuilder.add(logEntry.getLog());
                BufferedReader reader = logEntryMap.remove(logEntry);
                String nextLog = reader.readLine();
                if (nextLog != null) {
                    LogEntry nextLogEntry = new LogEntry(nextLog);
                    logEntryMap.put(nextLogEntry, reader);
                    sortedLogEntries.add(nextLogEntry);
                }
            }

            return sortedLogStreamBuilder.build();
        } catch (IOException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        } finally {
            FileSystemUtils.deleteRecursively(Paths.get(CHUNK_DIRECTORY));
        }
    }

    private void writeLogsInChunkToFile(Chunk chunk) throws IOException {
        File chunkedFile = new File(CHUNK_DIRECTORY + CHUNK_FILE_PREFIX + chunk.getChunkNo());
        IOUtils.writeLogsToFile(chunk.getLogs(), chunkedFile);
    }
}
